var hero;
var airplanes;
var animations;
var bees;
var bullets;
var point;//游戏进程
var game_status;//0:未开始	1:正在运行	2:已结束	3:暂停
var score;
var music = new Music;
function standby(){
	game_status = 0;
	setTimeout(function(){
		if(back_img.complete && hero0_img.complete && start_img.complete){
			hero = new Hero();
			airplanes = [];
			animations = [];
			bees = [];
			bullets = [];
			score = 0;

			paint_back();
			hero.paint();
			paint_start();
			
		}else standby();
	},20);
}

var timer = 0;
function start(){
	game_status = 1;

	point = setInterval(function(){
		paint_back();
		paint_enemy();
		paint_animation();
		paint_bullet();
		hero.paint();
		if(hero.life < 1){
			gameover();
		}
		hero_shoot();
		paint_info();
		timer++;
	}, 30);

}

function gameover() {
	game_status = 2;
	clearInterval(point);
	paint_over();
}

function paint_enemy(){
	if(timer%30 == 0){
		if(Math.random() * 10 > 8)
			bees.push(new Bee());
		else
			airplanes.push(new Plane());
	}
	loop_enemy(bees);
	loop_enemy(airplanes);
}

function paint_animation() {
	for (var i = 0; i < animations.length; i++) {
		if(!animations[i].paint()) {
			animations.splice(i, 1);
			i--;
		}
	}
}

function loop_enemy(array){
	for(i=0;i<array.length;i++){
		if((impact(array[i], hero) && array[i].life < 1) || out(array[i])){
			array.splice(i, 1);
			i--;
			continue;
		}
		array[i].paint();
	}
}

function paint_bullet(){
	next:
	for(l=0;l<bullets.length;l++){
		for(i=0;i<bees.length;i++){
			if((impact(bees[i], bullets[l]) && bullets[l].life < 1) || out(bullets[l])){
				bullets.splice(l, 1);
				if(bees[i].life < 1)
					reward(bees.splice(i,1)[0]);
				continue next;
			}
		}
		for(j=0;j<airplanes.length;j++){
			if((impact(airplanes[j], bullets[l]) && bullets[l].life < 1) || out(bullets[l])){
				bullets.splice(l, 1);
				if(airplanes[j].life < 1){
					var a = airplanes.splice(j,1)[0];
					score += 10;
					music.boomAudio();
					animations.push(new Animation(explosion, a.x, a.y));
				}
				continue next;
			}
		}
		bullets[l].paint();
	}
}

function reward(bee){
	music.rewardAudio();
	var img;
	switch (bee.cookie) {
		case 1:
			hero.life++;
			img = reward_life;
			break;
		case 2:
			hero.dbnum += 40;
			img = reward_bullet;
			break;
		default:
	}
	animations.push(new Animation(Array(20).fill(img), bee.x +20, bee.y + 20))
}

function hero_shoot() {
	if(timer%15 == 0){
		switch (hero.firemode) {
			case 2:
				db_fire();
				break;
			default:
				nomal_fire();
		}
	}
	music.bulletAudio();
}

function db_fire(){
	if (hero.dbnum > 0) {
		bullets.push(new Bullet(hero.x + hero.width/2 + 20 - bullet_img.width/2, hero.y - bullet_img.height));
		bullets.push(new Bullet(hero.x + hero.width/2 - 20 - bullet_img.width/2, hero.y - bullet_img.height));
		hero.dbnum-=2;
		music.bulletAudio();
	}else {
		hero.firemode = 0;
	}
}

function nomal_fire(){
	bullets.push(new Bullet(hero.x + hero.width/2 - bullet_img.width/2, hero.y - bullet_img.height));
}

function paint_info() {
	paint_score();
	paint_life();
	paint_dbfire();
}

/*************************碰撞算法**********************************************/
function impact(enemy, friend){
	var spaceX = friend.x + friend.width/2 - enemy.x - enemy.width/2;
	var spaceY = friend.y + friend.height/2 - enemy.y - enemy.height/2;

	spaceX = spaceX<0?-spaceX:spaceX;
	spaceY = spaceY<0?-spaceY:spaceY;
	if(spaceX < (enemy.width/2 + friend.width/2) &&
		spaceY < (enemy.height/2 + friend.height/2)){
		friend.life--;
		enemy.life--;
		return true;
	}
	return false;
}

/**********************超出场地***************************************************/
function out(o) {
	if(o.y < -o.height || o.y > $("#game").height()){
		return true;
	}
	return false;
}


function paint_back(){
	pre_img(back_img, 0, timer % back_img.height);
	pre_img(back_img, 0, -back_img.height + timer % back_img.height);
}
function paint_pause(){
	pre_img(pause_img);
}
function paint_start(){
	pre_img(start_img);
}
function paint_over(){
	pre_img(over_img);
}
function paint_score(){
	pre_txt("分数：" + score, 20, 40);
}
function paint_life(){
	pre_txt("生命：" + hero.life, 20, $("#game").height()-40);
}
function paint_dbfire(){
	pre_txt("双响炮：" + hero.dbnum, $("#game").width() - 120, $("#game").height()-40);
}
