$("#game").mouseenter(function(){
	if(game_status == 3){
		start();
	}
});
$("#game").mouseleave(function(){
	if(game_status == 1){
		game_status = 3;
		clearInterval(point);
		paint_pause();
	}
});
$("#game").mousemove(function(e){
	if(game_status == 1){
		hero.y = e.pageY - $(this).offset().top - hero.height/2;
		hero.x = e.pageX - $(this).offset().left - hero.width/2;
	}
});
$("#game").click(function(){
	command();
});
$("body").keydown(function(event) {
	switch (event.which) {
		case 32://空格
			command();
			break;
		default:
	}
});
function command(){
	if(game_status == 0){
		start();
	} else if(game_status == 2){
		standby();
	} else if (game_status == 1) {
		if (hero.firemode == 0) {
			hero.firemode = 2;
		}else if (hero.firemode == 2) {
			hero.firemode = 0;
		}
	}
}

//预备备~
standby();
