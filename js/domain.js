var start_img = new Image();
start_img.src = "img/start.png";
var airplane_img = new Image();
airplane_img.src = "img/airplane.png";
var back_img = new Image();
back_img.src = "img/background.png";
var hero0_img = new Image();
hero0_img.src = "img/hero0.png";
var hero1_img = new Image();
hero1_img.src = "img/hero1.png";
var bee_img = new Image();
bee_img.src = "img/bee.png";
var bullet_img = new Image();
bullet_img.src = "img/bullet.png";
var over_img = new Image();
over_img.src = "img/gameover.png";
var pause_img = new Image();
pause_img.src = "img/pause.png";

var reward_life = new Image();
reward_life.src = "img/reward_life.png";

var reward_bullet = new Image();
reward_bullet.src = "img/reward_bullet.png";

var explosion = [];
for(var i = 1; i < 20; i++) {
	var explosioni = new Image();
	explosioni.src = "img/explosion" + i + ".png"
	explosion.push(explosioni)
}

var game = document.getElementById("game");
var cxt=game.getContext("2d");
var game_speed = 4;//游戏倍速

/***************************    基础绘图方法    ******************************************/
function pre_img(img,x,y) {
	if(!x){x=0;}
	if(!y){y=0;}
	cxt.drawImage(img,x,y);
}
/***************************    基础写字方法    ******************************************/
function pre_txt(text,x,y) {
	cxt.font = "20px 黑体";
	cxt.fillStyle = "#000";
	cxt.fillText(text,x,y);
}
/*********************************Hero*************************************************/
function Hero() {
	this.height = hero0_img.height;
	this.width = hero0_img.width;

	this.x = 200;
	this.y = 620;

	this.life = 3;
	this.firemode = 0;
	this.dbnum = 0;

	this.img_index = 0;
	this.img = [hero0_img,hero1_img];
	this.paint = function(){
		pre_img(this.img[Math.floor(this.img_index++/10)%this.img.length], this.x, this.y);
	};

}
/*********************************plane************************************************/
function Plane() {
	this.height = airplane_img.height;
	this.width = airplane_img.width;
	this.life = 1;

	this.speed = 1 * game_speed;
	this.x = Math.random() * ($("#game").width() - this.width);
	this.y = 0 - this.height/2;
	this.paint = function(){
		pre_img(airplane_img, this.x, this.y += this.speed);
	};

}
/*********************************Bee**************************************************/
function Bee() {
	this.height = bee_img.height;
	this.width = bee_img.width;
	this.life = 2;
	this.cookie = Math.random() * 10 < 3 ? 1 : 2;

	this.speed = 1 * game_speed;
	this.x = Math.random() * ($("#game").width() - this.width);
	this.y = 0 - this.height/2;
	this.paint = function(){
		if(this.x < 0 || this.x > ($("#game").width() - this.width))
			this.speed = -this.speed;
		pre_img(bee_img, this.x += this.speed, this.y += this.speed>0?this.speed:-this.speed);
	};

}
/********************************Bullet************************************************/
function Bullet(x,y) {
	this.height = bullet_img.height;
	this.width = bullet_img.width;
	this.life = 1;

	this.speed = -8 * game_speed;
	this.x = x;
	this.y = y;
	this.paint = function(){
		pre_img(bullet_img, this.x, this.y += this.speed);
	};

}
/*********************************Music************************************************/
function Music() {
	this.bgm = new Audio();
	this.bgm.src = "audio/bgm.mp3";
	this.bgm.loop = true;
	this.bgm.autoplay = true;
	
	this.boom = new Audio();
	this.boom.src = "audio/boom.mp3";
	
	this.bullet = new Audio();
	this.bullet.src = "audio/bullet.mp3";
	
	this.reward = new Audio();
	this.reward.src = "audio/reward.mp3";
	
	this.boomAudio = function() {
		this.boom.currentTime = 0;
		this.boom.play();
	}
	
	this.bulletAudio = function () {
		this.bullet.play();
	}
	
	this.rewardAudio = function () {
		this.reward.currentTime = 0;
		this.reward.play();
	}
}
/*********************************Music************************************************/

function Animation(imgs, x, y){
	this.index = 0;
	this.imgs = imgs;
	this.x = x;
	this.y = y;
	
	this.paint = function(){
		if (this.index == this.imgs.length) {
			return false;
		}
		pre_img(this.imgs[this.index++], this.x, this.y);
		return true;
	}
}